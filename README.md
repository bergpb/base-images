## Container custom base-images

Build custom container images that will be used in personal projects and push to Gitlab Registry.


How to:
1. Build, tag and push using command: `make TAG="version" multiarch`