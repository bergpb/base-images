BUILDER_NAME     := registry.gitlab.com/bergpb/cotacaodiariabot/slim-bullseye-builder
BUILDER_TAG      := ${TAG}
BUILDER_IMG      := ${BUILDER_NAME}:${BUILDER_TAG}
BUILDER_LATEST   := ${BUILDER_NAME}:latest

LOCALES_NAME      := registry.gitlab.com/bergpb/cotacaodiariabot/slim-bullseye-locales
LOCALES_TAG       := ${TAG}
LOCALES_IMG       := ${LOCALES_NAME}:${LOCALES_TAG}
LOCALES_LATEST    := ${LOCALES_NAME}:latest

multiarch:
	docker buildx build --push --platform linux/amd64,linux/arm64 --tag ${BUILDER_IMG} -f ./builder/Dockerfile ./builder;
	docker buildx build --push --platform linux/amd64,linux/arm64 --tag ${BUILDER_LATEST} -f ./builder/Dockerfile ./builder;
	docker buildx build --push --platform linux/amd64,linux/arm64 --tag ${LOCALES_IMG} -f ./locales/Dockerfile ./locales;
	docker buildx build --push --platform linux/amd64,linux/arm64 --tag ${LOCALES_LATEST} -f ./locales/Dockerfile ./locales;

